FROM debian:wheezy

RUN apt-get update && apt-get install -y \
  build-essential \
  curl \
  git \
  gnupg \
  libc6-dev \
  libpq-dev \
  libcurl4-openssl-dev \
  libmemcached-dev \
  python-dev \
  python-virtualenv \
  wkhtmltopdf \
  xvfb \
  libxml2-dev libxslt1-dev

RUN echo \
  'xvfb-run --server-args="-screen 0, 1024x768x24" /usr/bin/wkhtmltopdf $*' > \
  /usr/bin/wkhtmltopdf.sh
RUN chmod a+rx /usr/bin/wkhtmltopdf.sh
RUN ln -s /usr/bin/wkhtmltopdf.sh /usr/local/bin/wkhtmltopdf

RUN curl --silent --show-error --retry 5 \
  https://raw.github.com/pypa/pip/master/contrib/get-pip.py | python2.7

RUN mkdir -p \
  /env \
  /project \
  /results

WORKDIR project

ADD scripts/ /scripts/

ENV TEST_FOLDERS unittests integrationtests
ENV VIRTUALENV_DEST_DIR /env

CMD ["/scripts/setup-build-project.sh"]

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
