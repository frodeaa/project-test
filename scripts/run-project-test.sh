#!/usr/bin/env bash

#
# run-project-test.sh \
#   --link psql:psql \
#   --link rmq:rmq \
#   -e TEST_FOLDERS="unittests integrationtests" \
#   -e DB_NAME=MYDB

set -e

function copy_result() {
  set +x
  local test_suffix=${test_name:-all}
  cd "/tmp/$JOB_NAME/results/"
  for f in *.*;
  do
    cp "$f" "$WORKSPACE/${f%.*}-${test_suffix}.${f##*.}";
  done
}

set -ex
DEFAULT_ENV_PATH="/tmp/$JOB_NAME/env"
ENV_PATH="${TEST_ENV_PATH-$DEFAULT_ENV_PATH}"
docker run \
  --name ${JOB_NAME} \
  --rm=true \
  $@ \
  --volume $WORKSPACE:/project \
  --volume $ENV_PATH:/env \
  --volume /tmp/$JOB_NAME/results:/results \
  dsafe/project-test

copy_result
