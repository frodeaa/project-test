#!/usr/bin/env bash

# run-pylint.sh app

curl -s $pylint_cfg > $WORKSPACE/pylint.cfg

PATHS=$@

ENV_PATH="/tmp/$JOB_NAME/env"
docker run \
  --name ${JOB_NAME} \
  --rm=true \
  --volume $WORKSPACE:/project \
  --volume $ENV_PATH:/env \
  --volume /tmp/$JOB_NAME/results:/results \
  dsafe/project-test sh -c \
  "virtualenv /env && \
   /env/bin/pip install pylint && \
   /env/bin/pip install -r /project/requirements.txt && \
   /env/bin/pylint --output-format=parseable \
     --rcfile=/project/pylint.cfg ${PATHS} \
   2>&1 | tee /results/pylint.log || exit 0"

cp "/tmp/$JOB_NAME/results/pylint.log" "$WORKSPACE/pylint.log"
