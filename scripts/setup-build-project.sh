#!/usr/bin/env bash

virtualenv "${VIRTUALENV_DEST_DIR}"

# install project requirements
"${VIRTUALENV_DEST_DIR}/bin/pip" install \
  --find-links "$(pwd)" \
  --log-file /results/pip.log \
  -r /project/requirements.txt

# install test dependencies
"${VIRTUALENV_DEST_DIR}/bin/pip" install \
  --log-file /results/pip-testdep.log \
  nose \
  nosexcover

# run tests
env
"${VIRTUALENV_DEST_DIR}/bin/nosetests" \
 --traverse-namespace \
 --with-xunit \
 --xunit-file=/results/nosetests.xml \
 -v ${TEST_FOLDERS} 2>&1 | tee /results/nosetests.log
