#!/usr/bin/env bash

name=$1
port=$2
image=$3

set -x

docker stop $name 1>&- 2>&-
docker rm -f $name 1>&- 2>&-

set -ex
docker run \
  --name $name -p "${port}:${port}" -d ${image}
