#!/usr/bin/env bash

#
# ./create_db git@bitbucket.org:dsafe/secure.git createdb.sql user database
#

remote=$1
sql_file=$2
db_user=$3
db_name=$4
db_container=psql
sql_folder=${5:-""}

git archive \
  --format=tar \
  --remote=$remote HEAD:$sql_folder \
  -- $sql_file | tar xf -

docker run \
  -e PGPASSWORD=cowabunga \
  -v $(pwd):/tmp \
  --rm \
  --link $db_container:$db_container \
  dsafe/pg \
  /bin/bash -c \
  "psql -h $db_container -U $db_user -d $db_name \
    -c 'drop schema public cascade; create schema public;' && \
   psql -h $db_container -U $db_user -d $db_name -f /tmp/$sql_file"
