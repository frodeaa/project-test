# project-test

A Dockerfile for testing a python application

## Build the docker file

    docker build -t dsafe/project-test .

## Test a project with Jenkins

Run the docker container with project volume mounted

    trap 'docker rm $JOB_NAME' 0

    docker run --name $JOB_NAME \
      -v $WORKSPACE:/project \
      -v /tmp/$JOB_NAME/env:/env \
      dsafe/project-test

### Complex example

Run nosetest on a project, mount virtualenv folder and
result to make them available after completing the test

    docker run --name $JOB_NAME \
    --rm=true \
    --link psql:psql \
    --link rabbitmq:rmq \
    --volume $WORKSPACE:/project \
    --volume /tmp/$JOB_NAME/env:/env \
    --volume /tmp/$JOB_NAME/results:/results \
    dsafe/project-test
    cp /tmp/$JOB_NAME/results/*.* .
