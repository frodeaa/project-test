#!/bin/bash
echo "******CREATING DOCKER DATABASE******"
gosu postgres postgres --single <<- EOSQL
   CREATE DATABASE unittests;
   CREATE DATABASE testcarddb;
   CREATE DATABASE unittests_provider;
   CREATE DATABASE unittests_receiptparser;
   CREATE USER unittests WITH SUPERUSER PASSWORD 'cowabunga';
   CREATE USER testuser WITH SUPERUSER PASSWORD 'testtest';
   CREATE USER unittests_secure WITH SUPERUSER PASSWORD 'cowabunga';
   CREATE USER provider WITH PASSWORD 'cowabunga';
   CREATE USER receiver WITH PASSWORD 'cowabunga';
   CREATE USER retail WITH PASSWORD 'cowabunga';
   CREATE USER admin WITH PASSWORD 'cowabunga';
   CREATE USER sso WITH PASSWORD 'cowabunga';
   CREATE USER vasadmin WITH PASSWORD 'cowabunga';
   CREATE USER readonly WITH PASSWORD 'cowabunga';
   CREATE USER webserver WITH PASSWORD 'cowabunga';
   CREATE USER distributor WITH PASSWORD 'cowabunga';
   CREATE USER tryeng WITH PASSWORD 'cowabunga';
   CREATE USER madsh WITH PASSWORD 'cowabunga';
   CREATE USER sven WITH PASSWORD 'cowabunga';
EOSQL
echo ""
echo "******DOCKER DATABASE CREATED******"
